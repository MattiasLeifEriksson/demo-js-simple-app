# Simple Demo JS App

This is a demo app that anyone call `clone` and then play around with. The app has several views, use the navbar on the homepage to cycle through them.

## Install

```
git clone https://gitlab.com/thelunararmy/demo-js-simple-app
cd demo-js-simple-app
npm install
```

## Usage

```
npm start
```

This will open a new Webpage in your browser at `localhost:3000`. Remember to use your React and Redux browser extentions

## Contributors 

[JC Bailey (@thelunararmy)](@thelunararmy)

## Contributing

No contributions allowed. I am the boss here. 😼

## Licence 

Copyright 2022, Noroff Accelerate AS
