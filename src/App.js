import { useContext, useEffect } from 'react';
import './App.css';
import ProfileCard from './components/ProfileCard';
import SimpleForm from './components/SimpleForm';
import { BrowserRouter, Routes, Route, NavLink } from 'react-router-dom';

import { ProfileContext } from './hoc/ProfileContext';
import CounterPage from './components/CounterPage';
import PicturePage from './components/PicturePage';
// import UserProfile from './components/UserProfile';

function App() {
  const [ , setProfile ] = useContext(ProfileContext)
  useEffect(
    () => async () => { 
      fetch("https://randomuser.me/api/")
        .then( response => response.json() )
        .then( _json => _json.results[0] )
        .then( data => setProfile({
          username: data.login.username,
          picture: data.picture.thumbnail
        }))
        .catch( e => console.log(e) ) }, [ setProfile ]
  )


  return (
    <BrowserRouter>
      <div className="App">
        <h1>My demo app</h1>
        <Routes>
          <Route path="/profile" element={<ProfileCard />} />
          <Route path="/form" element={<SimpleForm />} />   
          <Route path="/counter" element={<CounterPage />} />
          <Route path="/pictures" element={<PicturePage />} />         
        </Routes>
        <footer>
          <h4>Navigation:</h4>
          <nav>
            <li><NavLink to="/profile">Goto Profile 🧑‍🦳</NavLink></li>
            <li><NavLink to="/form">Goto Form 📝</NavLink></li>
            <li><NavLink to="/counter">Goto Counter 🧱</NavLink></li>
            <li><NavLink to="/pictures">Goto Pictures 📸</NavLink></li>
          </nav>
        </footer>
      </div>
    </BrowserRouter>
  );
}

export default App;
