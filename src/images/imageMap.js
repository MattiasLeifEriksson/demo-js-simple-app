import gem01 from "./01.PNG"
import gem02 from "./02.PNG"
import gem03 from "./03.PNG"
import gem04 from "./04.PNG"
import gem05 from "./05.PNG"
import gem06 from "./06.PNG"

const gemImages = {
    a : gem01,
    b : gem02,
    c : gem03,
    d : gem04,
    e : gem05,
    f : gem06,
}

const getImagePath = (imagename) => {
    return gemImages[imagename]
}

export default getImagePath