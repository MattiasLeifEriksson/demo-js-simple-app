
function UserProfileCard (props) {
    const {picture, username, country, name} = props.userdata
    return (
        <>
            <img src={picture} alt="Profile"></img>
            <p>{username}</p>
            <p>{country}</p>
            <p>{name}</p> 
        </>
    )
}

export default UserProfileCard