export const increment = () => ({
    type: "[count] INCREMENT",
})

export const decrement = () => ({
    type: "[count] DECREMENT",
})

export const boost = (offset = 1) => ({
    type: "[count] BOOST",
    payload: offset,
})

export const superboost = () => ({
    type: "[count] SUPER_BOOST",
})