export function countReducer(state = 0, action){
    switch (action.type) {
        case "[count] INCREMENT":
            return state + 1;
        case "[count] DECREMENT":
            return state - 1;
        case "[count] BOOST":
            return state + action.payload;
        default:
            return state
    }
}